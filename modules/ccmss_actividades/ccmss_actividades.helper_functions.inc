<?php

function ccmss_actividades_calculate_age($nacimiento) {
  $birth_year = $nacimiento[0]['value'];
  $current_year = date('Y');
  $age = (int)$current_year - (int)$birth_year;

  return $age;
}
