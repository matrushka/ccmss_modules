(function ($) {
  Drupal.behaviors.ccmssActividadesHierarchy = {
    attach: function (context, settings) {
      this.bindActividadesHierarchy(context, settings);
    },
    bindActividadesHierarchy: function(context, settings) {
      // Monkey patch the Hierarchy Select code that unbinds all events
      // disregarding other modules
      if(!this.hsMonkeyPatched) {
        // Make a copy of the attachBindings function provided by the HS module
        Drupal.behaviors.ccmssActividadesHierarchy.hsBindings = Drupal.HierarchicalSelect.attachBindings;

        // Rewire the attachBindings function in the HS module to a function on our own
        // to be able to add bindings after HS has done an .unbind() on its selects
        Drupal.HierarchicalSelect.attachBindings = function(hsid) {
          Drupal.behaviors.ccmssActividadesHierarchy.attachHierarchicalSelectBindings(hsid, context, settings);
        }

        this.hsMonkeyPatched = true;
      }

    },
    attachHierarchicalSelectBindings: function(hsid, context, settings) {
      var ccmssActividadesHierarchy = this;
      // Call our copy of the attachBindings function. This is the standard HS behavior.
      ccmssActividadesHierarchy.hsBindings(hsid);

      // Bind the grupos hierarchical select
      $('#edit-field-at-grupo-nucleo select:eq(0)', context).change(function() {
        ccmssActividadesHierarchy.updateAvailableActiviades($(this));        
      });

    },
    updateAvailableActiviades: function($select) {
      var grupo = $select.val();
      var mapping = Drupal.settings.ccmss.actividades_grupos;

      var $actividadOptions = $('#edit-field-at-actividad-und option');

      $actividadOptions.each(function() {
        var $option = $(this);
        var tidActividad = $option.val();

        var gruposActividad = mapping[tidActividad];
        
        // The default state is enabled for all options
        $option.removeAttr('disabled');
        
        // If an option tid has groups setting and 
        if(typeof(gruposActividad) != 'undefined' && Array.isArray(gruposActividad) && gruposActividad.indexOf(grupo) == -1) {
          $option.attr('disabled', 'disabled');
        }
      });
    },

  };
}(jQuery));
