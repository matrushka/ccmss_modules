<?php
/*
 * Renders the hoja de tiempo table of actividades and horas. 
 * Hoja de tiempo gets the following variables:
 *
 * @param user
 * Drupal user object
 *
 * @param start
 * string with start date in Y-m-d format
 * 
 * @param end
 * string with end date in Y-m-d format
 * 
 * @param actividades
 * Array that contains the full actividades taxonomy tree
 *
 * @param tiempos
 * Array with objects representing time registered for activities and days
 *
 * @param dias_no_laborados
 * Array with all the días no laborados (not just start and end but all the days in range)
 *
 * @param days
 * Array with strings for all the days contained in the period
 * 
 * @param months
 * Array with strings for all the months contained in the period, localized
 */


$start_formatted = format_date(strtotime($start), 'custom', 'd-M-Y');
$end_formatted = format_date(strtotime($end), 'custom', 'd-M-Y');
?>

<div class="hoja-de-tiempo-header">Hoja de tiempo de <span class="user"><?php print $user->name; ?></span> del <span class="start-date"><?php print $start_formatted; ?></span> al <span class="end-date"><?php print $end_formatted; ?></span>.</div>

<div class="hoja-de-tiempo-table"><?php print _ccmss_render_hoja_de_tiempo_table($user, $start, $end, $actividades, $tiempos, $dias_no_laborados, $days, $months); ?></div>

