<?php


function _ccmss_tiempo_title($node) {
  if(empty($node->field_tiempo_persona['und'])) {
    global $user;
    $username = $user->name;
  }
  else {
    $subject_id = $node->field_tiempo_persona['und']['0']['target_id'];
    $user = user_load($subject_id);
    $username = $user->name;
  }

  $date = $node->field_tiempo_fecha['und'][0]['value'];
  $date = substr($date, 0, 10);
  
  return "Tiempo de $username el $date";
}

function _ccmss_dnl_title($node) {
  if(empty($node->field_tiempo_persona['und'])) {
    global $user;
    $username = $user->name;
  }
  else {
    $subject_id = $node->field_tiempo_persona['und']['0']['target_id'];
    $user = user_load($subject_id);
    $username = $user->name;
  }

  $date = $node->field_dnl_fecha['und'][0]['value'];
  $date = substr($date, 0, 10);

  $date_end = $node->field_dnl_fecha['und'][0]['value2'];
  $date_end = substr($date_end, 0, 10);

  if($date == $date_end) {
    return "Día no laborado de $username en $date";
  }
  else {
    return "Día(s) no laborado(s) de $username del $date al $date_end";
  }
}

function _responsive_bartik_menu_menu_tiempo_block_visibility() {
  $alias_components = explode('/', drupal_get_path_alias());
  if($alias_components[0] == 'tiempo') {
    return true;
  }

  // Whenever the user is visiting a tiempo-related path
  if(arg(0) == 'tiempo') {
    return true;
  }
  
  // when the user is editing a tiempo or dia no laborado
  if(arg(0) == 'node' && arg(2) == 'edit') {
    $node = node_load(arg(1));
    if($node->type == 'tiempo' || $node->type == 'dia_no_laborado') {
      return true;
    }
  }
  
  // when a user is adding tiempo or día no laborado
  if(arg(0) == 'node' && arg(1) == 'add' && (arg(2) == 'tiempo' || arg(2) == 'dia-no-laborado')) {
    return true;
  }
  
  // When the actividades taxonomy tree is being managed
  if(arg(2) == 'taxonomy' && arg(3) == 'actividades') {
    return true;
  }
}

function _responsive_bartik_menu_menu_actividades_block_visibility() {
  $alias_components = explode('/', drupal_get_path_alias());
  if($alias_components[0] == 'actividades') {
    return true;
  }

  // Whenever the user is visiting a actividades-related path
  if(arg(0) == 'actividades') {
    return true;
  }

  // when the user is editing a taller or asistencia técnica
  if(arg(0) == 'node' && arg(2) == 'edit') {
    $node = node_load(arg(1));
    if($node->type == 'taller' || $node->type == 'asistencia_tecnica') {
      return true;
    }
  }

  // when a user is adding talleres or asistencias técnicas
  if(arg(0) == 'node' && arg(1) == 'add' && (arg(2) == 'taller' || arg(2) == 'asistencia-tecnica')) {
    return true;
  }
}
